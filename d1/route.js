const http = require("http");

//create a variable "port" to store the port number
const port = 4000;

//creat a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request,response)=> {
	if(request.url == `/greeting`){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Hello again")
	} else if (request.url == "/homepage"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("This is the homepage")
	} else{
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("Page not available")
	}
});

//use the "server" and "port" variable created above
server.listen(port)

console.log(`Server now accessible at localhost:${port}.`);