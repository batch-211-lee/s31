//Node JS

//Use the "require" directive to load Node.js modules
//A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the  Hyper Text Transfer Protocol
//the "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
//HTTP is a protocol that allows the fetching of resources such as HTML document.
//Clients (browsers) and servers (Node.js/Express.js applications) communicate by exchanging individual messages
// The messages sent by the client, usually, the web browser, are called requests
//the messages sent by the server as an answer are called responses
let http = require("http");

// the http module has a createServer method that accepts a function as an argument and allows for a creation of a server
//The arguments passed in the function are request and response objects that contains methods that allow us to receive requests from the client and send responses back to it

/*
	use the writeHead() method to
	-set a status code for the respons- a 200 means ok
	-Set the content-type of the response as a plain text message
*/

//send the response with a text content "Hello World"

http.createServer(function(request, response){
	response.writeHead(200, {"Content-Type": "text/plain"})
	response.end("Hello World")
}).listen(4000)

//a port is a virtual point where network connections start and end
// each port is associated with a specific process or service
//the server will be assigned for port 4000 via the ".listen(4000)" method where the server will listen to any requests that are sent to it, eventually communicating with our server.

console.log("Server running at localhost: 4000")